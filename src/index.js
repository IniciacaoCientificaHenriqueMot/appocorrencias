import React from 'react';
import { YellowBox } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';

// import './config/ReactotronConfig';
import Main from './main';

import { store } from './store';

YellowBox.ignoreWarnings(['Setting a timer']);

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#569690',
    accent: '#569690',
    background: '#fff',
    text: '#000',
  },
};

export default function App() {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <Main />
      </PaperProvider>
    </Provider>
  );
}
