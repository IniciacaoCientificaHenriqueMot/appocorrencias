import styled from 'styled-components/native';
import Button from '~/components/button';

export const SafeArea = styled.SafeAreaView`
  flex: 1;
`;

export const Container = styled.View`
  flex: 1;
  padding: 20px;
  justify-content: flex-end;
  background: #ece5dd;
`;

export const Title = styled.Text`
  font-size: 30px;
  text-transform: uppercase;
`;

export const Header = styled.View`
  align-items: center;
  margin-bottom: 20;
`;

export const Main = styled.View`
  flex: 2;
`;

export const ButtonLogout = styled(Button)`
  align-self: center;
  width: 100px;
  margin-top: 20px;
  background: tomato;
  border-radius: 4px;
`;

export const ButtonChangePassword = styled(Button)`
  margin-top: 20px;
`;

export const TextButtonLogout = styled.Text`
  font-size: 18px;
  text-transform: uppercase;
  color: #fff;
`;
