import React, { useState } from 'react';
import { KeyboardAvoidingView, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {
  Avatar,
  Subheading,
  TextInput,
  Title,
  withTheme,
} from 'react-native-paper';
import { Actions } from 'react-native-router-flux';
import { useSelector } from 'react-redux';
import { useFirestoreAuth } from '~/services/firestore-auth';
import {
  Container,
  SafeArea,
  Header,
  TextButtonLogout,
  ButtonLogout,
  ButtonChangePassword,
} from './styles';

function Profile() {
  const { fullName, email } = useSelector(state => state.loggedUser);
  const { counter: loading } = useSelector(state => state.loading);
  const firstLetter = fullName.substring(0, 1).toUpperCase();
  const { signOut } = useFirestoreAuth();
  const [userInfo, setUserInfo] = useState({
    fullName: '',
    email: '',
    password: '',
  });
  const [newPassword, setNewPassword] = useState({
    pass: '',
    newPass: '',
    confirmPass: '',
  });

  function handleSubmit() {
    signOut().then(() => {
      Actions.reset('auth');
    });
  }

  return (
    <KeyboardAvoidingView
      enabled
      behavior="padding"
      keyboardVerticalOffset={80}
      style={{ flex: 1 }}
    >
      <SafeArea>
        <ScrollView>
          <Container>
            <Header>
              <Avatar.Text size={70} label={firstLetter} />
              <Title>{fullName}</Title>
            </Header>
            <Subheading>Dados gerais</Subheading>
            <TextInput
              style={{ backgroundColor: '#ece5dd' }}
              label="Nome completo"
              mode="flat"
              disabled
              value={fullName}
              onChangeText={text =>
                setUserInfo({ ...userInfo, fullName: text })
              }
            />
            <TextInput
              style={{ backgroundColor: '#ece5dd' }}
              label="E-mail"
              mode="flat"
              disabled
              value={email}
            />
            <Subheading style={{ marginTop: 20 }}>Alterar Senha</Subheading>
            <TextInput
              style={{ backgroundColor: '#ece5dd' }}
              label="Senha atual"
              placeholder="Digite sua senha atual"
              mode="flat"
              value={newPassword.pass}
              onChangeText={text =>
                setUserInfo({ ...userInfo, fullName: text })
              }
            />
            <TextInput
              style={{ backgroundColor: '#ece5dd' }}
              label="Nova senha"
              placeholder="Digite a nova senha"
              mode="flat"
              value={newPassword.newPass}
            />
            <TextInput
              style={{ backgroundColor: '#ece5dd' }}
              label="Confirmar nova senha"
              placeholder="Confirmar a nova senha"
              mode="flat"
              value={newPassword.confirmPass}
            />
            <ButtonChangePassword
              onPress={() =>
                Alert.alert('Sucesso', 'Senha alterada com sucesso!')
              }
            >
              Alterar senha
            </ButtonChangePassword>
            <ButtonLogout loading={loading} onPress={handleSubmit}>
              <TextButtonLogout>Sair</TextButtonLogout>
            </ButtonLogout>
          </Container>
        </ScrollView>
      </SafeArea>
    </KeyboardAvoidingView>
  );
}

export default withTheme(Profile);
