import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { ActivityIndicator, Title } from 'react-native-paper';
import { useSelector } from 'react-redux';
import OccurrenceCard from '~/components/occurrence-card';
import useFirestoreQuery from '~/hooks/use-firestore-query';
import { firebaseFirestore } from '~/utils/firebase';
import { Container, LoadingContainer, SafeArea } from './styles';

export default function MinhasOcorrencias() {
  const { uid } = useSelector(state => state.loggedUser);

  const ref = firebaseFirestore
    .collection('occurrences')
    .where('denunciante.uid', '==', uid)
    .orderBy('dataHoraOcorrencia', 'desc');
  const { isLoading, data: occurrences } = useFirestoreQuery(ref);

  return (
    <SafeArea>
      {isLoading ? (
        <LoadingContainer>
          <ActivityIndicator size="large" />
        </LoadingContainer>
      ) : (
        <ScrollView style={{ backgroundColor: '#ece5dd' }}>
          <Container>
            {occurrences && occurrences.docs.length > 0 ? (
              occurrences.docs.map(doc => (
                <OccurrenceCard
                  key={doc.id}
                  date={
                    doc.data().dataHoraOcorrencia
                      ? doc.data().dataHoraOcorrencia.substring(0, 10)
                      : '2019-10-06'
                  }
                  description={doc.data().descricao}
                  tags={doc.data().tiposOcorrencia}
                />
              ))
            ) : (
              <Title>Nenhuma ocorrência cadastrada</Title>
            )}
          </Container>
        </ScrollView>
      )}
    </SafeArea>
  );
}
