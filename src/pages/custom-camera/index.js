import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Text, View } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Actions } from 'react-native-router-flux';
import { Camera } from 'expo-camera';
import { Container, CameraView } from './styles';
import CameraToolbar from '~/components/camera-toolbar';
import Galeria from '~/components/galeria/galeria';
import { addMediaCaptures } from '~/store/modules/ocorrencia/action';

export default function CustomCamera() {
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [cameraRef, setCameraRef] = useState(null);
  const [captures, setCaptures] = useState([]);
  const [capturing, setCapturing] = useState(null);

  const { Type: CameraTypes } = Camera.Constants;

  const [type, setType] = useState(CameraTypes.back);

  const dispatch = useDispatch();

  async function solicitarPermissaoCamera() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    setHasCameraPermission(status === 'granted');
  }

  useEffect(() => {
    solicitarPermissaoCamera();
  });

  function handleAddCaptures() {
    dispatch(addMediaCaptures(captures));
    Actions.pop();
  }

  if (hasCameraPermission === null) {
    return <View />;
  }

  if (hasCameraPermission === false) {
    return <Text>Sem acesso à câmera</Text>;
  }

  function handleCaptureIn() {
    setCapturing(true);
  }

  function handleCameraType() {
    setType(type === CameraTypes.back ? CameraTypes.front : CameraTypes.back);
  }

  const onShortCapture = async () => {
    if (cameraRef) {
      const photoData = await cameraRef.takePictureAsync({ quality: 0.5 });
      setCaptures([...captures, photoData]);
      setCapturing(false);
    }
  };

  return (
    <Container>
      <CameraView
        ref={ref => setCameraRef(ref)}
        style={{ flex: 1, justifyContent: 'flex-end' }}
        skipProcessing
        type={type}
      >
        {captures.length > 0 && <Galeria captures={captures} />}
        <CameraToolbar
          onShortCapture={onShortCapture}
          confirmSendAction={handleAddCaptures}
          onPressIn={handleCaptureIn}
          capturing={capturing}
          handleCameraType={handleCameraType}
        />
      </CameraView>
    </Container>
  );
}
