import styled from 'styled-components/native';
import { Camera } from 'expo-camera';

export const Container = styled.View`
  display: flex;
  flex: 1;
`;

export const CameraView = styled(Camera)`
  flex: 1;
  justify-content: flex-end;
`;

export const ControlBar = styled.View`
  flex: 0.2;
  flex-direction: row;
  align-items: center;
  background: #000;
`;

export const ControlBarLeft = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
`;

export const ControlBarCenter = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
`;

export const ControlBarRight = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
`;

export const FlipButton = styled.TouchableOpacity``;

export const FlipButtonText = styled.Text`
  font-size: 18px;
  margin-bottom: 10px;
  color: #fff;
`;
