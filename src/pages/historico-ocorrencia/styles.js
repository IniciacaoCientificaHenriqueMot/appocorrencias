import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding: 20px;
  justify-content: center;
  align-items: center;
  background: #ece5dd;
`;

export const Title = styled.Text`
  font-size: 30px;
  text-transform: uppercase;
`;
