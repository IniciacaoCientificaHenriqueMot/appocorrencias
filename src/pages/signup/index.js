import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Alert } from 'react-native';
import { withTheme } from 'react-native-paper';
import { Actions } from 'react-native-router-flux';
import Button from '~/components/button';

import { Container, Form, TInput, Title } from './styles';
import { useFirestoreAuth } from '~/services/firestore-auth';

function SignUp(props) {
  const firebaseAuth = useFirestoreAuth();
  const { counter: loading } = useSelector(state => state.loading);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [fullName, setFullName] = useState('');

  const {
    theme: { colors },
  } = props;

  function handleSubmit() {
    const erroMsg = firebaseAuth.registerUser(email, password, fullName, () => {
      Actions.jump('ocorrencia', { fullName, email });
    });
    if (erroMsg) {
      Alert.alert(erroMsg);
    }
  }

  return (
    <Container backgroundColor={colors.background}>
      <Title color={colors.primary}>Ocorrências</Title>
      <Form>
        <TInput
          label="Digite seu nome completo"
          mode="flat"
          value={fullName}
          onChangeText={text => setFullName(text)}
        />
        <TInput
          label="Digite seu e-mail"
          mode="flat"
          keyboardType="email-address"
          value={email}
          onChangeText={text => setEmail(text)}
        />
        <TInput
          label="Digite sua senha secreta"
          mode="flat"
          value={password}
          secureTextEntry
          onChangeText={text => setPassword(text)}
        />
        <Button loading={loading} onPress={handleSubmit}>
          Criar conta
        </Button>
      </Form>
    </Container>
  );
}

export default withTheme(SignUp);
