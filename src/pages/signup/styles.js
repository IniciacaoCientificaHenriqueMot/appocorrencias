import styled from 'styled-components/native';
import { TextInput } from 'react-native-paper';
import Button from '~/components/button';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
  background: ${props => props.backgroundColor};
`;

export const Form = styled.KeyboardAvoidingView.attrs({
  enable: true,
  behavior: 'padding',
})`
  align-self: stretch;
  margin-top: 50px;
`;

export const TInput = styled(TextInput)`
  margin-bottom: 10px;
  background: #fff;
`;

export const SigninButton = styled(Button)`
  padding: 5px;
  margin-top: 5px;
`;

export const Title = styled.Text`
  font-size: 40px;
  text-transform: uppercase;
  color: ${props => props.color};
`;

export const SignInLink = styled.TouchableOpacity`
  padding: 5px;
  margin-top: 15px;
`;

export const TextSignInLink = styled.Text`
  font-size: 18px;
  color: ${props => props.color}
  text-align: center;
`;
