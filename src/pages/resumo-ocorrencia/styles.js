import styled from 'styled-components/native';
import Button from '~/components/button';

export const Container = styled.ScrollView`
  flex: 1;
  padding: 20px;
  background: #ece5dd;
`;

export const EnviarButton = styled(Button)`
  margin-top: 20px;
  margin-bottom: 50px;
`;
