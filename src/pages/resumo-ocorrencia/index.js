import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { resetOccurrence } from '~/store/modules/ocorrencia/action';
import Card from '~/components/card';

import { Container, EnviarButton } from './styles';
import { useFirestore } from '~/services/firestore-service';

export default function ResumoOcorrencia() {
  const dispatch = useDispatch();
  const ocorrencia = useSelector(state => state.ocorrencia);
  const { saveOccurrence } = useFirestore();

  const { tiposOcorrencia } = ocorrencia;

  let tiposString = '';
  let data = null;

  tiposOcorrencia.forEach((t, index) => {
    if (index < tiposOcorrencia.length - 1) {
      tiposString = tiposString.concat(t).concat(', ');
    } else {
      tiposString = tiposString.concat(t);
    }
  });

  if (ocorrencia.registrosMidia.length !== 0) {
    data = [
      { title: 'Tipos', contentTitle: tiposString },
      { title: 'Descrição', contentTitle: ocorrencia.descricao },
      {
        title: 'Nº de mídias',
        contentTitle:
          ocorrencia.registrosMidia.length !== 0
            ? ocorrencia.registrosMidia.length
            : '',
      },
    ];
  } else {
    data = [
      { title: 'Tipos', contentTitle: tiposString },
      { title: 'Descrição', contentTitle: ocorrencia.descricao },
    ];
  }

  const dataLocation = [
    { title: 'Latitude', contentTitle: ocorrencia.latitude },
    { title: 'Longitude', contentTitle: ocorrencia.longitude },
  ];

  function handleSubmit() {
    saveOccurrence(ocorrencia);
    dispatch(resetOccurrence());
    Actions.reset('ocorrencia');
    Actions.jump('minhasOcorrencias');
  }

  return (
    <Container>
      <Card data={data} dataLocation={dataLocation} />
      <EnviarButton onPress={handleSubmit}>Enviar ocorrência</EnviarButton>
    </Container>
  );
}
