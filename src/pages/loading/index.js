import React, { useEffect } from 'react';
import { Text, ActivityIndicator } from 'react-native-paper';
import { Actions } from 'react-native-router-flux';
import { Container, NormalText } from './styles';
import { firebaseAuth } from '~/utils/firebase';

export default function Loading() {
  useEffect(() => {
    firebaseAuth.onAuthStateChanged(user => {
      if (user) {
        Actions.jump('ocorrencia');
      } else {
        Actions.jump('auth');
      }
    });
  });

  return (
    <Container>
      <NormalText>Carregando...</NormalText>
      <ActivityIndicator size="large" />
    </Container>
  );
}
