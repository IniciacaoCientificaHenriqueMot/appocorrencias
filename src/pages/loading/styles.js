import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const NormalText = styled.Text`
  font-size: 22;
  margin-bottom: 20;
`;
