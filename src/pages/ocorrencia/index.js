import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { Avatar, Card, Switch, TextInput, withTheme } from 'react-native-paper';
import { Actions } from 'react-native-router-flux';
import { useDispatch, useSelector } from 'react-redux';
import CaptureBox from '~/components/capture-box';
import If from '~/components/if';
import OccurrenceType from '~/components/occurrence-type';
import useFirestoreQuery from '~/hooks/use-firestore-query';
import { useFirestoreAuth } from '~/services/firestore-auth';
import {
  addOccurrence,
  removeMediaCapture,
  updateOccurrenceLocation,
} from '~/store/modules/ocorrencia/action';
import { firebaseFirestore } from '~/utils/firebase';
import {
  Container,
  EnviarButton,
  IconButton,
  LocationButton,
  NormalText,
  Row,
  TypesContainer,
} from './styles';

function Ocorrencia(props) {
  const dispatch = useDispatch();

  const { getCurrentUser } = useFirestoreAuth();

  const user = useSelector(state => state.loggedUser);

  const refOccurrenceTypes = firebaseFirestore.collection('occurrence-types');
  const { isLoading, data: occurrenceTypes } = useFirestoreQuery(
    refOccurrenceTypes
  );

  const { registrosMidia, formatted_address: formattedAddress } = useSelector(
    state => state.ocorrencia
  );

  const [dadosOcorrencia, setDadosOcorrencia] = useState({
    tiposOcorrencia: [],
    descricao: '',
    registrosMidia: [],
    formattedAddress: '',
  });

  const [errorMessage, setErrorMessage] = useState(null);

  const [isHere, setIsHere] = useState(true);

  async function getLocationAsync() {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      setErrorMessage('Permissão para acessar a localização foi negada');
    }
    const location = await Location.getCurrentPositionAsync({});
    setDadosOcorrencia({
      ...dadosOcorrencia,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    });
    dispatch(updateOccurrenceLocation(location.coords));
  }

  useEffect(() => {
    getCurrentUser();
    getLocationAsync();
  }, []);

  function handleSubmit() {
    dispatch(addOccurrence(dadosOcorrencia));
    Actions.resumoOcorrencia();
  }

  function addOccurrenceType(occurrenceType) {
    setDadosOcorrencia({
      ...dadosOcorrencia,
      tiposOcorrencia: [...dadosOcorrencia.tiposOcorrencia, occurrenceType],
    });
  }

  function removeOccurrenceType(occurrenceType) {
    const copyTypes = dadosOcorrencia.tiposOcorrencia;
    const updatedTypes = copyTypes.filter(value => value !== occurrenceType);
    setDadosOcorrencia({ ...dadosOcorrencia, tiposOcorrencia: updatedTypes });
  }

  return (
    <Container>
      <Card style={{ marginBottom: 10 }}>
        <Card.Title
          title={user && user.fullName}
          subtitle={user && user.email}
          left={props => <Avatar.Icon {...props} icon="person" />}
        />
      </Card>
      <TypesContainer>
        {occurrenceTypes &&
          occurrenceTypes.docs.map(doc => (
            <OccurrenceType
              key={doc.data().description}
              description={doc.data().description}
              actionAdd={() => addOccurrenceType(doc.data().description)}
              actionRemove={() => removeOccurrenceType(doc.data().description)}
            />
          ))}
      </TypesContainer>

      <TextInput
        label="Breve descrição"
        mode="outlined"
        style={{ backgroundColor: '#ece5dd' }}
        multiline
        numberOfLines={3}
        value={dadosOcorrencia.descricao}
        onChangeText={text =>
          setDadosOcorrencia({
            ...dadosOcorrencia,
            descricao: text,
          })
        }
      />

      {errorMessage && <Text style={{ color: 'red' }}>{errorMessage}</Text>}

      <Row>
        <IconButton
          onPress={() => Actions.customCamera()}
          name="photo-camera"
          size={35}
          color="#234a46"
        />
        <Switch
          style={{ marginLeft: 5 }}
          value={isHere}
          onValueChange={() => setIsHere(!isHere)}
        />
        <NormalText>Estou no local</NormalText>
        {!isHere && (
          <LocationButton onPress={() => Actions.map()}>
            Informar
          </LocationButton>
        )}
      </Row>

      <If condition={formattedAddress}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 10,
            padding: 10,
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,0.5)',
            borderStyle: 'solid',
            borderRadius: 5,
          }}
        >
          <Text>{formattedAddress}</Text>
        </View>
      </If>

      {registrosMidia && registrosMidia.length > 0 && (
        <CaptureBox items={registrosMidia} removeAction={removeMediaCapture} />
      )}
      <EnviarButton
        onPress={() => {
          handleSubmit();
        }}
      >
        Próximo
      </EnviarButton>
    </Container>
  );
}

export default withTheme(Ocorrencia);
