import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Button from '~/components/button';
import Input from '~/components/input';

export const SafeArea = styled.SafeAreaView`
  flex: 1;
`;

export const Container = styled.ScrollView`
  flex: 1;
  margin-top: 20px;
  padding: 20px;
  background: #ece5dd;
`;

export const TInput = styled(Input)`
  align-items: flex-start;
  margin-top: 20px;
  height: 120px;
  border: 1px solid rgba(0, 0, 0, 0.3);
  border-radius: 4px;
  margin-bottom: 10px;
`;

export const CustomPicker = styled.Picker`
  margin-top: 20px;
`;

export const IconButton = styled(Icon)``;

export const IconButtonLocation = styled(Icon)`
  margin-left: 10px;
`;

export const EnviarButton = styled(Button)`
  margin-top: 20px;
  margin-bottom: 50px;
`;

export const LocationButton = styled(Button)`
  flex: 1;
  padding-left: 5px;
  padding-right: 5px;
  padding-top: 0;
  padding-bottom: 0;
  margin-left: 10px;
  margin-top: 0;
  margin-bottom: 0;
`;

export const Row = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin-top: 10px;
`;

export const NormalText = styled.Text`
  font-size: 16px;
`;

export const TypesContainer = styled.View`
  flex: 1;
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 5px;
  margin-top: 5;
`;
