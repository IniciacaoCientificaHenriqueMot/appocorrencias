import React, { useState } from 'react';
import { Image, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { updateOccurrenceLocation } from '~/store/modules/ocorrencia/action';
import SearchPlaces from '~/components/search-places';

import {
  Container,
  ConfirmButtom,
  ButtomContainer,
  ButtomSubContainer,
} from './styles';
import GOOGLE_APIKEY from '~/services/google-apikey';

export default function Map() {
  const widthScreen = Dimensions.get('window').width;
  const heightScreen = Dimensions.get('window').height;

  const dispatch = useDispatch();
  const ocorrencia = useSelector(state => state.ocorrencia);

  const [location, setLocation] = useState({
    latitude: ocorrencia ? ocorrencia.latitude : -3.7424701,
    longitude: ocorrencia ? ocorrencia.longitude : -38.503942,
    formatted_address: '',
  });

  function handleLocationSelected(data, { geometry, formatted_address }) {
    const {
      location: { lat: latitude, lng: longitude },
    } = geometry;
    setLocation({ latitude, longitude, formatted_address });
  }

  function handleSubmit() {
    dispatch(updateOccurrenceLocation(location));
    Actions.pop();
  }

  return (
    <Container>
      <Image
        style={{
          flex: 1,
          padding: 20,
          backgroundColor: 'skyblue',
        }}
        source={{
          uri: `https://maps.googleapis.com/maps/api/staticmap?zoom=16&size=${widthScreen}x${heightScreen}&maptype=roadmap
            &markers=color:red%7Clabel:S%7C${location.latitude},${location.longitude}&key=${GOOGLE_APIKEY}`,
        }}
        resizeMode="cover"
      />
      <SearchPlaces handleLocationSelected={handleLocationSelected} />
      <ButtomContainer>
        <ButtomSubContainer>
          <ConfirmButtom onPress={handleSubmit}>Confirmar</ConfirmButtom>
        </ButtomSubContainer>
      </ButtomContainer>
    </Container>
  );
}
