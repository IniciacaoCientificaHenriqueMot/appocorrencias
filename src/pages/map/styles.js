import styled from 'styled-components/native';
import Button from '~/components/button';

export const Container = styled.View`
  flex: 1;
  background: darkgreen;
`;

export const ButtomContainer = styled.View`
  position: absolute;
  bottom: 20px;
  width: 100%;
`;

export const ButtomSubContainer = styled.View`
  flex: 1;
  margin-left: 20px;
  margin-right: 20px;
`;

export const ConfirmButtom = styled(Button)``;
