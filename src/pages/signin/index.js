import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { withTheme } from 'react-native-paper';
import { Actions } from 'react-native-router-flux';
import Button from '~/components/button';
import {
  Container,
  Form,
  TInput,
  Title,
  SignInLink,
  TextSignInLink,
} from './styles';
import { useFirestoreAuth } from '~/services/firestore-auth';

function SignIn(props) {
  const { signInUser } = useFirestoreAuth();
  const { counter: loading } = useSelector(state => state.loading);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const {
    theme: { colors },
  } = props;

  function handleSubmit() {
    signInUser(email, password, () => {
      Actions.jump('ocorrencia');
    });
  }

  return (
    <Container backgroundColor={colors.background}>
      <Title color={colors.primary}>Ocorrências</Title>
      <Form>
        <TInput
          label="Digite seu e-mail"
          mode="flat"
          keyboardType="email-address"
          value={email}
          onChangeText={text => setEmail(text)}
        />
        <TInput
          label="Digite sua senha"
          mode="flat"
          value={password}
          secureTextEntry
          onChangeText={text => setPassword(text)}
        />
        <Button loading={loading} onPress={handleSubmit}>
          Entrar
        </Button>
        <SignInLink onPress={() => Actions.signup()}>
          <TextSignInLink color={colors.primary}>
            Criar uma conta
          </TextSignInLink>
        </SignInLink>
      </Form>
    </Container>
  );
}

export default withTheme(SignIn);
