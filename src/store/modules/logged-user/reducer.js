import produce from 'immer';

const INITIAL_STATE = {};

export default function loggedUser(state = INITIAL_STATE, action) {
  switch (action.type) {
    case '@loggedUser/REGISTER_USER':
      return {
        uid: action.payload.user.uid,
        fullName: action.payload.fullName,
        email: action.payload.user.email,
      };
    case '@loggedUser/LOADING_USER':
      return {
        ...state,
        uid: action.payload.uid,
        fullName: action.payload.simpleUser.fullName,
        email: action.payload.simpleUser.email,
      };
    case '@loggedUser/UPDATE_USER_OCCURRENCES':
      return produce(state, draft => {
        if (draft.occurrences && draft.occurrences > 0) {
          draft.occurrences.push(...action.payload);
        } else {
          draft.occurrences = action.payload;
        }
      });
    case '@loggedUser/RESET_USER':
      return {
        uid: '',
        fullName: '',
        email: '',
      };
    default:
      return state;
  }
}
