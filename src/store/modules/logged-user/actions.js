export function registerUser(user, fullName) {
  return {
    type: '@loggedUser/REGISTER_USER',
    payload: { user, fullName },
  };
}

export function loadingUser(uid, simpleUser) {
  return {
    type: '@loggedUser/LOADING_USER',
    payload: { uid, simpleUser },
  };
}

export function resetUser() {
  return {
    type: '@loggedUser/RESET_USER',
  };
}

export function updateUserOccurrences(occurrences) {
  return {
    type: '@loggedUser/UPDATE_USER_OCCURRENCES',
    payload: occurrences,
  };
}
