import { useDispatch } from 'react-redux';

export function useLoading() {
  const dispatch = useDispatch();
  return {
    loadingStart: () => {
      dispatch({ type: '@loading/START' });
    },
    loadingStop: () => {
      dispatch({ type: '@loading/STOP' });
    },
  };
}
