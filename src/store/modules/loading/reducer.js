const INITIAL_STATE = {
  counter: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case '@loading/START':
      return { ...state, counter: state.counter + 1 };
    case '@loading/STOP':
      if (state.counter > 1) {
        return { ...state, counter: state.counter - 1 };
      }
      return { ...state, counter: 0 };
    default:
      return state;
  }
};
