import { combineReducers } from 'redux';

import ocorrencia from './ocorrencia/reducer';
import loggedUser from './logged-user/reducer';
import loading from './loading/reducer';

export default combineReducers({
  ocorrencia,
  loggedUser,
  loading,
});
