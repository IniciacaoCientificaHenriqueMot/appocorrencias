import { all } from 'redux-saga/effects';

import ocorrencia from './ocorrencia/sagas';

export default function* rootSaga() {
  return yield all([ocorrencia]);
}
