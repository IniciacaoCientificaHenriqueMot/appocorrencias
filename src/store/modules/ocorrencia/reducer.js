import produce from 'immer';
import moment from 'moment';

const INITIAL_STATE = {
  denunciante: null,
  tiposOcorrencia: [],
  descricao: '',
  registrosMidia: [],
  latitude: '',
  longitude: '',
  formatted_address: '',
};

export default function ocorrencia(state = INITIAL_STATE, action) {
  switch (action.type) {
    case '@ocorrencia/ADD_OCCURRENCE':
      return produce(state, draft => {
        draft.tiposOcorrencia = action.payload.tiposOcorrencia;
        draft.descricao = action.payload.descricao;
        draft.dataHoraOcorrencia = moment().format();
      });
    case '@ocorrencia/UPDATE_OCCURRENCE_LOCATION':
      return produce(state, draft => {
        draft.latitude = action.payload.latitude;
        draft.longitude = action.payload.longitude;
        draft.formatted_address = action.payload.formatted_address
          ? action.payload.formatted_address
          : '';
      });
    case '@ocorrencia/ADD_MEDIA_CAPTURES':
      return produce(state, draft => {
        draft.registrosMidia.push(...action.payload);
      });
    case '@ocorrencia/REMOVE_MEDIA_CAPTURE':
      return produce(state, draft => {
        const uri = action.payload;
        const mediaIndex = draft.registrosMidia.findIndex(r => r.uri === uri);
        if (mediaIndex >= 0) draft.registrosMidia.splice(mediaIndex, 1);
      });
    case '@ocorrencia/RESET_OCCURRENCE':
      return produce(state, draft => {
        draft.denunciante = null;
        draft.tiposOcorrencia = [];
        draft.descricao = '';
        draft.registrosMidia = [];
        draft.latitude = '';
        draft.longitude = '';
        draft.formatted_address = '';
        draft.dataHoraOcorrencia = '';
      });
    case '@ocorrencia/UPDATE_OCCURRENCE_USER':
      return produce(state, draft => {
        draft.denunciante = {
          uid: action.payload.uid,
          fullName: action.payload.simpleUser.fullName,
          email: action.payload.simpleUser.email,
        };
      });
    default:
      return state;
  }
}
