export function addOccurrence(occurrence) {
  return {
    type: '@ocorrencia/ADD_OCCURRENCE',
    payload: occurrence,
  };
}

export function updateOccurrenceUser(uid, simpleUser) {
  return {
    type: '@ocorrencia/UPDATE_OCCURRENCE_USER',
    payload: { uid, simpleUser },
  };
}

export function updateOccurrenceLocation(location) {
  return {
    type: '@ocorrencia/UPDATE_OCCURRENCE_LOCATION',
    payload: location,
  };
}

export function addMediaCaptures(captures) {
  return {
    type: '@ocorrencia/ADD_MEDIA_CAPTURES',
    payload: captures,
  };
}

export function removeMediaCapture(uri) {
  return {
    type: '@ocorrencia/REMOVE_MEDIA_CAPTURE',
    payload: uri,
  };
}

export function resetOccurrence() {
  return {
    type: '@ocorrencia/RESET_OCCURRENCE',
  };
}
