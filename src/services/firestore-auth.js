import { useDispatch } from 'react-redux';
import {
  registerUser,
  loadingUser,
  resetUser,
} from '~/store/modules/logged-user/actions';
import { updateOccurrenceUser } from '~/store/modules/ocorrencia/action';
import { firebaseAuth, firebaseFirestore } from '../utils/firebase';
import { errorHandler } from '~/utils/errorHandler';
import { useLoading } from '~/store/modules/loading/actions';

export function useFirestoreAuth() {
  const { loadingStart, loadingStop } = useLoading();
  const dispatch = useDispatch();

  function addMoreUserInfo(infoObject) {
    const { email, fullName } = infoObject;
    firebaseAuth.onAuthStateChanged(user => {
      if (user) {
        const { uid } = user;
        firebaseFirestore
          .collection('users')
          .add({ email, fullName, uid })
          .catch(error => {
            errorHandler(error);
          });
        dispatch(registerUser(user, fullName, uid));
      }
    });
  }

  return {
    registerUser: (email, password, fullName, callback = null) => {
      loadingStart();
      firebaseAuth
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          addMoreUserInfo({ email, fullName });
        })
        .then(loadingStop)
        .then(callback)
        .catch(error => {
          loadingStop();
          errorHandler(error);
        });
    },

    signInUser: (email, password, callback) => {
      loadingStart();
      firebaseAuth
        .signInWithEmailAndPassword(email, password)
        .then(loadingStop)
        .then(callback)
        .catch(error => {
          loadingStop();
          errorHandler(error);
        });
    },

    signOut: () => {
      loadingStart();
      dispatch(resetUser());
      return firebaseAuth.signOut().then(loadingStop);
    },

    getCurrentUser: async () => {
      if (firebaseAuth.currentUser) {
        const { uid } = firebaseAuth.currentUser;
        firebaseFirestore
          .collection('users')
          .where('uid', '==', uid)
          .get()
          .then(querySnapshot => {
            querySnapshot.forEach(doc => {
              dispatch(loadingUser(uid, doc.data()));
              dispatch(updateOccurrenceUser(uid, doc.data()));
            });
          })
          .catch(error => {
            errorHandler(error);
          });
      }
    },
  };
}
