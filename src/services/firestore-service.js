import { useDispatch } from 'react-redux';
import { firebaseFirestore } from '~/utils/firebase';
import { errorHandler } from '~/utils/errorHandler';
import { updateUserOccurrences } from '~/store/modules/logged-user/actions';

export function useFirestore() {
  const dispatch = useDispatch();

  return {
    saveOccurrence: occurrence => {
      firebaseFirestore
        .collection('occurrences')
        .add(occurrence)
        .catch(error => {
          errorHandler(error);
        });
    },

    listByUser: userId => {
      const list = [];
      firebaseFirestore
        .collection('occurrences')
        .where('denunciante.uid', '==', userId)
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach(doc => {
            list.push(doc.data());
          });
          dispatch(updateUserOccurrences(list));
        })
        .catch(error => {
          errorHandler(error);
        });
    },

    listeningOccurrencesByUser: userId => {
      const list = [];
      firebaseFirestore
        .collection('occurrences')
        .where('denunciante.uid', '==', userId)
        .onSnapshot(querySnapshot => {
          querySnapshot.forEach(doc => {
            list.push(doc.data());
          });
          dispatch(updateUserOccurrences(list));
        });
    },
  };
}
