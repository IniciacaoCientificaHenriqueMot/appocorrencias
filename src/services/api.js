/* eslint-disable no-unused-vars */
import axios from 'axios';

const ipDaMinhaMaquina = '127.0.0.1';

const iosEmulatorURL = 'http://localhost:3333';
const androidStudioEmulatorURL = 'http://10.0.2.2:3333';
const genymotionEmulatorURL = 'http://10.0.3.2:3333';
const usbURL = `http://${ipDaMinhaMaquina}:3333`;

const api = axios.create({
  baseURL: `${androidStudioEmulatorURL}`,
});

export default api;
