import { firebaseDatabase } from '../utils/firebase';

export default class FirebaseService {
  static saveOccurrence(occurrence) {
    const occurrences = firebaseDatabase.ref('occurrences');
    const occurrenceKey = occurrences.push().key;
    occurrences.child(occurrenceKey).set(occurrence);
  }

  static getOccurrencesById() {
    firebaseDatabase.ref('occurrences').on('value', snapshot => {
      snapshot.forEach(childItem => {
        return childItem.val().denunciante;
      });
    });
  }
}
