import { useDispatch } from 'react-redux';
import { registerUser, loadingUser } from '~/store/modules/logged-user/actions';
import { updateOccurrenceUser } from '~/store/modules/ocorrencia/action';
import { firebaseDatabase, firebaseAuth } from '../utils/firebase';
import { errorHandler } from '~/utils/errorHandler';

export function useFirebaseAuth() {
  const dispatch = useDispatch();

  function addMoreUserInfo(infoObject) {
    const { email, fullName } = infoObject;

    firebaseAuth.onAuthStateChanged(user => {
      if (user) {
        firebaseDatabase
          .ref('users')
          .child(user.uid)
          .set({
            email,
            fullName,
          });
        dispatch(registerUser(user, fullName));
      }
    });
  }

  return {
    registerUser: (email, password, fullName, callback = null) => {
      firebaseAuth
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          addMoreUserInfo({ email, fullName });
        })
        .then(callback)
        .catch(error => {
          errorHandler(error);
        });
    },

    signInUser: (email, password, callback) => {
      firebaseAuth
        .signInWithEmailAndPassword(email, password)
        .then(callback)
        .catch(error => {
          errorHandler(error);
        });
    },

    signOut: () => {
      return firebaseAuth.signOut();
    },

    getCurrentUser: async () => {
      if (firebaseAuth.currentUser) {
        const { uid } = firebaseAuth.currentUser;
        firebaseDatabase
          .ref('users')
          .child(uid)
          .once('value')
          .then(snapshot => {
            dispatch(loadingUser(uid, snapshot.val()));
            dispatch(updateOccurrenceUser(uid, snapshot.val()));
          });
      }
    },
  };
}
