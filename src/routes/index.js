import React from 'react';
import { Router, Stack, Scene, Tabs } from 'react-native-router-flux';

import Ocorrencia from '~/pages/ocorrencia';
import CustomCamera from '~/pages/custom-camera';
import Map from '~/pages/map';
import ResumoOcorrencia from '~/pages/resumo-ocorrencia';
import MinhasOcorrencias from '~/pages/minhas-ocorrencias';
import HistoricoOcorrencia from '~/pages/historico-ocorrencia';
import Profile from '~/pages/profile';
import TabIcon from '~/components/tab-icon';
import SignIn from '~/pages/signin';
import SignUp from '~/pages/signup';
import Loading from '~/pages/loading';

export default function Routes() {
  return (
    <Router>
      <Scene key="root">
        <Scene key="loading" initial component={Loading} hideNavBar />
        <Scene key="auth" hideNavBar>
          <Scene key="signin" component={SignIn} title="Sign in" hideNavBar />
          <Scene key="signup" component={SignUp} title="Sign up" hideNavBar />
        </Scene>
        <Scene hideNavBar>
          <Tabs
            key="tabs"
            activeTintColor="#fff"
            inactiveTintColor="rgba(255, 255, 255, 0.6)"
            tabBarStyle={{ backgroundColor: '#569791' }}
            headerTintColor="#fff"
          >
            <Stack
              key="ocorrenciaTab"
              title="Denunciar"
              navigationBarStyle={{ backgroundColor: '#569791' }}
              titleStyle={{ color: '#fff', textTransform: 'uppercase' }}
              iconName="announcement"
              icon={TabIcon}
            >
              <Scene
                key="ocorrencia"
                component={Ocorrencia}
                title="Denunciar"
                hideNavBar
              />
              <Scene
                key="customCamera"
                component={CustomCamera}
                title="Camera"
                hideTabBar
                hideNavBar
              />
              <Scene key="map" component={Map} title="Localização" hideTabBar />
              <Scene
                key="resumoOcorrencia"
                component={ResumoOcorrencia}
                title="Resumo"
              />
            </Stack>
            <Stack
              key="minhasOcorrenciasTab"
              title="Ocorrências"
              navigationBarStyle={{ backgroundColor: '#569791' }}
              titleStyle={{ color: '#fff', textTransform: 'uppercase' }}
              iconName="list"
              icon={TabIcon}
            >
              <Scene
                key="minhasOcorrencias"
                component={MinhasOcorrencias}
                title="Ocorrências"
              />
              <Scene
                key="historicoOcorrencia"
                component={HistoricoOcorrencia}
                title="Histórico"
              />
            </Stack>
            <Stack
              key="perfilTab"
              title="Perfil"
              navigationBarStyle={{ backgroundColor: '#569791' }}
              titleStyle={{ color: '#fff', textTransform: 'uppercase' }}
              iconName="person"
              icon={TabIcon}
            >
              <Scene key="perfil" component={Profile} title="Perfil" />
            </Stack>
          </Tabs>
        </Scene>
      </Scene>
    </Router>
  );
}
