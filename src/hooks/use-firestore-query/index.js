import { useState, useEffect } from 'react';

export default function useFirestoreQuery(ref) {
  const [docState, setDocState] = useState({
    isLoading: true,
    data: null,
  });

  useEffect(() => {
    return ref.onSnapshot(docs => {
      setDocState({
        isLoading: false,
        data: docs,
      });
    });
  }, []);

  return docState;
}
