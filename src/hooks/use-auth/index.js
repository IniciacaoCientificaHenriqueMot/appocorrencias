import { useState, useEffect } from 'react';

export default function useAuth(auth) {
  const [authState, setAuthState] = useState({
    isLoading: true,
    user: null,
  });

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(authChanged =>
      setAuthState({ isLoading: false, user: authChanged })
    );
    return unsubscribe;
  }, [auth]);

  return authState;
}
