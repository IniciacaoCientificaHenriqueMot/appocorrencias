import createRoutes from './routes';

export default function Main() {
  return createRoutes();
}
