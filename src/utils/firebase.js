import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Your web app's Firebase configuration
// Firebase Database Realtime ---------------------
// const prodConfig = {
//   apiKey: 'AIzaSyCPE-UoLM1nogcwT6HsRCB4TuMP1zemccc',
//   authDomain: 'app-ocorrencias.firebaseapp.com',
//   databaseURL: 'https://app-ocorrencias.firebaseio.com',
//   projectId: 'app-ocorrencias',
//   storageBucket: '',
//   messagingSenderId: '74236521142',
//   appId: '1:74236521142:web:401fafb1d9dba897e05417',
//   measurementId: 'G-0SM3T0Q2XG',
// };

// const devConfig = {
//   apiKey: 'AIzaSyCPE-UoLM1nogcwT6HsRCB4TuMP1zemccc',
//   authDomain: 'app-ocorrencias.firebaseapp.com',
//   databaseURL: 'https://app-ocorrencias.firebaseio.com',
//   projectId: 'app-ocorrencias',
//   storageBucket: '',
//   messagingSenderId: '74236521142',
//   appId: '1:74236521142:web:401fafb1d9dba897e05417',
//   measurementId: 'G-0SM3T0Q2XG',
// };
// Firebase Database Realtime ---------------------

// Firebase Firestore -----------------------------

// const prodConfig = {
//   apiKey: 'AIzaSyB8vNoC4oQGlC9lvYvG3gP9RsyCR2GAK3k',
//   authDomain: 'ocorrencias-a6ddd.firebaseapp.com',
//   databaseURL: 'https://ocorrencias-a6ddd.firebaseio.com',
//   projectId: 'ocorrencias-a6ddd',
//   storageBucket: 'ocorrencias-a6ddd.appspot.com',
//   messagingSenderId: '1096186532194',
//   appId: '1:1096186532194:web:652143ee1efd198c891c3b',
//   measurementId: 'G-085NKQ957B',
// };

const devConfig = {
  apiKey: 'AIzaSyB8vNoC4oQGlC9lvYvG3gP9RsyCR2GAK3k',
  authDomain: 'ocorrencias-a6ddd.firebaseapp.com',
  databaseURL: 'https://ocorrencias-a6ddd.firebaseio.com',
  projectId: 'ocorrencias-a6ddd',
  storageBucket: 'ocorrencias-a6ddd.appspot.com',
  messagingSenderId: '1096186532194',
  appId: '1:1096186532194:web:652143ee1efd198c891c3b',
  measurementId: 'G-085NKQ957B',
};

// Firebase Firestore -----------------------------

// const config = process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
const config = devConfig;

// Initialize Firebase
export const firebaseImpl = firebase.initializeApp(config);
// export const firebaseDatabase = firebase.database();
export const firebaseFirestore = firebase.firestore();
export const firebaseAuth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
export const facebookProvider = new firebase.auth.FacebookAuthProvider();
