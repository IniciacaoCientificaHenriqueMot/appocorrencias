import { Alert } from 'react-native';

export const errorHandler = error => {
  const { code } = error;
  switch (code) {
    case 'auth/email-already-in-use':
      Alert.alert('Atenção', 'E-mail já cadastrado');
      break;
    case 'auth/invalid-email':
      Alert.alert('Atenção', 'E-mail inválido');
      break;
    case 'auth/weak-password':
      Alert.alert('Atenção', 'Senha muito fraca');
      break;
    case 'auth/user-disabled':
      Alert.alert('Atenção', 'Usuário inválido');
      break;
    case 'auth/user-not-found':
      Alert.alert('Atenção', 'Usuário não encontrado');
      break;
    case 'auth/wrong-password':
      Alert.alert('Atenção', 'Senha incorreta');
      break;
    default:
      Alert.alert('Erro', 'Desculpe. Ocorreu um erro inesperado');
      break;
  }
};
