import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  margin: 20px 0;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 5px 0;
  border-bottom-width: 1px;
  border-bottom-color: rgba(0, 0, 0, 0.3);
  margin-bottom: 20px;
`;
