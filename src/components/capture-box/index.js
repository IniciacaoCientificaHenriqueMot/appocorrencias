import React from 'react';
import { useDispatch } from 'react-redux';
import { Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { FlatList } from 'react-native-gesture-handler';
import { Container, Row } from './styles';

export default function CaptureBox({ items, removeAction }) {
  const dispatch = useDispatch();

  return (
    <Container>
      <FlatList
        data={items}
        keyExtractor={item => item.uri}
        renderItem={({ item, index }) => (
          <Row>
            <Text>Mídia {index + 1}</Text>
            <Icon
              name="delete"
              size={25}
              color="tomato"
              onPress={() => dispatch(removeAction(item.uri))}
            />
          </Row>
        )}
      />
    </Container>
  );
}
