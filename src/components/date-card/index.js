import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.3)',
    borderStyle: 'solid',
    backgroundColor: '#569690',
  },
  day: {
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    borderStyle: 'solid',
    fontSize: 12,
    color: '#fff',
    fontWeight: 'bold',
  },
  month: {
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    borderStyle: 'solid',
    fontSize: 12,
    color: '#fff',
    fontWeight: 'bold',
  },
  year: {
    fontSize: 12,
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default function DateCard(props) {
  const { date } = props;
  const arrayDate = date.split('-');
  const [yearTemp, month, day] = arrayDate;
  const year = yearTemp.substring(2);
  return (
    <View style={styles.container}>
      <Text style={styles.day}>{day}</Text>
      <Text style={styles.month}>{month}</Text>
      <Text style={styles.year}>{year}</Text>
    </View>
  );
}
