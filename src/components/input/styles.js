import styled from 'styled-components/native';

export const Container = styled.View`
  padding: 0;
  height: 46px;
  border-bottom-width: 1px;
  border-bottom-color: rgba(0, 0, 0, 0.5);
  flex-direction: row;
  align-items: center;
`;

export const TInput = styled.TextInput.attrs({
  placeholderTextColor: 'rgba(0, 0, 0, 0.5)',
})`
  flex: 1;
  font-size: 16px;
  margin-left: 10px;
  color: #000;
`;
