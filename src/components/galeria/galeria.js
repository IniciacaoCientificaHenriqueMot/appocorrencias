import React from 'react';

import { Container, ImageContainer, Gallery } from './styles';

export default function Galeria({ captures = [] }) {
  return (
    <Container horizontal>
      {captures.map(({ uri }) => (
        <ImageContainer key={uri}>
          <Gallery source={{ uri }} />
        </ImageContainer>
      ))}
    </Container>
  );
}
