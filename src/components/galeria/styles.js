import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

const { width: winWidth } = Dimensions.get('window');

export const Container = styled.ScrollView`
  width: ${winWidth};
  position: absolute;
  height: 100px;
  bottom: 50px;
`;

export const ImageContainer = styled.View`
  width: 75px;
  height: 75px;
  margin-right: 2px;
`;

export const Gallery = styled.Image`
  width: 75px;
  height: 75px;
`;
