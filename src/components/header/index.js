import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  Container,
  ButtonContainer,
  TitleContainer,
  Text,
  BackButton,
} from './styles';

export default function Header(props) {
  // eslint-disable-next-line react/prop-types
  const { titulo, isFirstRouteInParent, goBack } = props;
  return (
    <Container>
      <ButtonContainer>
        {!isFirstRouteInParent() && (
          <BackButton onPress={() => goBack()}>
            <Icon name="arrow-back" size={20} color="#fff" />
          </BackButton>
        )}
      </ButtonContainer>
      <TitleContainer>
        <Text>{titulo}</Text>
      </TitleContainer>
    </Container>
  );
}
