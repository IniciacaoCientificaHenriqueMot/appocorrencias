import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.SafeAreaView`
  display: flex;
  flex-direction: row;
  height: 75px;
  background-color: #569791;
`;

export const ButtonContainer = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 20px;
`;

export const TitleContainer = styled.View`
  flex: 5;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
  padding-top: 25px;
`;

export const Text = styled.Text`
  font-size: 20;
  color: #ededed;
  text-transform: uppercase;
`;

export const BackButton = styled(RectButton)`
  padding: 15px;
`;
