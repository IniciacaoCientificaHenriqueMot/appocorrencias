import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { Container } from './styles';

export default function TabIcon(props) {
  const { iconName, focused } = props;
  const color = focused ? '#fff' : 'rgba(255, 255, 255, 0.6)';
  return (
    <Container>
      <Icon style={{ color }} name={iconName} size={20} />
    </Container>
  );
}
