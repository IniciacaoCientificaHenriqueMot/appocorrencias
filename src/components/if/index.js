import React from 'react';
import { View } from 'react-native';

export default function If({ condition, children }) {
  if (condition) {
    return <View>{children}</View>;
  }
  return null;
}
