import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import DateCard from '../date-card';
import Tag from '../tag';

const styles = StyleSheet.create({
  container: {
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.3)',
    borderStyle: 'solid',
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 10,
  },
  dateContainer: {
    flex: 0.2,
    alignItems: 'center',
    // borderRightWidth: 1,
    // borderStyle: 'solid',
    // borderRightColor: 'rgba(0, 0, 0, 0.3)',
    marginRight: 10,
    padding: 5,
  },
  infoContainer: {
    flex: 0.8,
    padding: 5,
    // backgroundColor: 'darkgreen',
  },
  description: {
    flex: 1,
    paddingLeft: 5,
    // backgroundColor: 'purple',
  },
  tags: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor: 'orange',
  },
  textDescription: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default function OccurrenceCard(props) {
  const { date, description, tags } = props;
  return (
    <View style={styles.container}>
      <View style={styles.dateContainer}>
        <DateCard date={date} />
      </View>
      <View style={styles.infoContainer}>
        <View style={styles.description}>
          <Text numberOfLines={1} style={styles.textDescription}>
            {description}
          </Text>
        </View>
        <View style={styles.tags}>
          {tags && tags.map(t => <Tag key={t}>{t}</Tag>)}
        </View>
      </View>
    </View>
  );
}
