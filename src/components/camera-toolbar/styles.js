import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 0.2;
`;

export const Row = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
`;

export const Col = styled.View`
  flex: 0.33;
  align-items: center;
`;

export const CaptureButtonInactive = styled.View`
  width: 60px;
  height: 60px;
  border-width: 2px;
  border-radius: 60px;
  border-color: #fff;
`;

export const CaptureButtonActive = styled.View`
  width: 70px;
  height: 70px;
  border-width: 2px;
  border-radius: 70px;
  border-color: #fff;
  background: #fff;
`;
