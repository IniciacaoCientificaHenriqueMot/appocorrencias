import React, { useState, useEffect } from 'react';
import { Alert } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import {
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native-gesture-handler';

import {
  Container,
  Row,
  Col,
  CaptureButtonInactive,
  CaptureButtonActive,
} from './styles';

export default function CameraToolbar({
  onShortCapture,
  confirmSendAction,
  capturing,
  onPressIn,
  handleCameraType,
}) {
  function sendConfirm() {
    Alert.alert('Confirmação de gravação', 'Confirma o gravação das fotos?', [
      {
        text: 'Não',
        style: 'cancel',
      },
      { text: 'Sim', onPress: confirmSendAction },
    ]);
  }

  return (
    <Container>
      <Row>
        <Col>
          <TouchableOpacity onPress={handleCameraType}>
            <Ionicon name="md-reverse-camera" size={25} color="#fff" />
          </TouchableOpacity>
        </Col>
        <Col>
          <TouchableWithoutFeedback
            onPress={onShortCapture}
            onPressIn={onPressIn}
          >
            {capturing ? <CaptureButtonActive /> : <CaptureButtonInactive />}
          </TouchableWithoutFeedback>
        </Col>
        <Col>
          <TouchableOpacity onPress={() => sendConfirm()}>
            <Ionicon name="md-send" size={25} color="#fff" />
          </TouchableOpacity>
        </Col>
      </Row>
    </Container>
  );
}
