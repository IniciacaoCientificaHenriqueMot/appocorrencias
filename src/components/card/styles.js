import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;

export const Container = styled.View`
  background: #fff;
  padding: 10px;
  border-width: 1px;
  border-color: rgba(0, 0, 0, 0.3);
  border-radius: 10px;
`;

export const Row = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  padding-bottom: 5px;
`;

export const Col = styled.View``;

export const TitleItem = styled.Text`
  font-size: 16px;
  color: rgba(0, 0, 0, 0.5);
  text-transform: uppercase;
`;

export const ContentItem = styled.Text`
  font-size: 16px;
  color: rgba(0, 0, 0, 1);
`;

export const MapContainer = styled.View`
  align-items: center;
  padding-bottom: 20px;
`;

export const Map = styled.Image`
  width: ${deviceWidth * 0.8};
  height: ${deviceWidth * 0.5};
`;
