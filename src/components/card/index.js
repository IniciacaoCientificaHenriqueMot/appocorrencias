import React from 'react';
import { Dimensions } from 'react-native';

import {
  Container,
  Row,
  Col,
  TitleItem,
  ContentItem,
  MapContainer,
  Map,
} from './styles';
import GOOGLE_APIKEY from '~/services/google-apikey';

export default function Card({ data, dataLocation }) {
  const widthScreen = Dimensions.get('window').width;
  const heightScreen = Dimensions.get('window').height;

  const [
    { contentTitle: latitude },
    { contentTitle: longitude },
  ] = dataLocation;

  return (
    <Container>
      <MapContainer>
        <Map
          source={{
            uri: `https://maps.googleapis.com/maps/api/staticmap?zoom=16&size=${widthScreen}x${heightScreen}&maptype=roadmap
            &markers=color:red%7Clabel:S%7C${latitude},${longitude}&key=${GOOGLE_APIKEY}`,
          }}
          resizeMode="cover"
        />
      </MapContainer>

      {data.map(item => (
        <Row key={item.title}>
          <Col>
            <TitleItem>{item.title}: </TitleItem>
          </Col>
          <Col>
            <ContentItem>{item.contentTitle}</ContentItem>
          </Col>
        </Row>
      ))}
    </Container>
  );
}
