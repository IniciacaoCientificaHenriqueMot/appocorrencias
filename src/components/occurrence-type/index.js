import React, { useState } from 'react';
import { Chip, withTheme } from 'react-native-paper';

function OccurrenceType({ description, theme, actionAdd, actionRemove }) {
  const [isSelected, setIsSelected] = useState(false);

  return (
    <Chip
      style={{ marginRight: 5, marginBottom: 5 }}
      mode="outlined"
      onPress={() => {
        setIsSelected(!isSelected);
        if (!isSelected) {
          actionAdd();
        } else {
          actionRemove();
        }
      }}
      selectedColor={isSelected ? theme.colors.primary : '#000'}
      selected={isSelected}
    >
      {description}
    </Chip>
  );
}

export default withTheme(OccurrenceType);
