/* eslint-disable react/prop-types */
import React from 'react';
import fotoUsuario from '../../assets/images/avatar01.png';
import { Container, Avatar, Info, Nome, Texto } from './styles';

export default function UsuarioCard({ user }) {
  return (
    <Container>
      <Avatar source={fotoUsuario} />
      <Info>
        <Nome>{user && user.fullName}</Nome>
        <Texto>{user && user.email}</Texto>
      </Info>
    </Container>
  );
}
