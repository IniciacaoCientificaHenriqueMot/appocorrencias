import styled from 'styled-components/native';

export const Container = styled.View`
  display: flex;
  flex-direction: row;
  padding: 10px;
`;

export const Info = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-left: 10px;
`;

export const Avatar = styled.Image`
  width: 64px;
  height: 64px;
  border-radius: 32px;
  background-color: #eee;
`;

export const Nome = styled.Text`
  font-weight: bold;
  font-size: 16px;
`;

export const Texto = styled.Text`
  font-size: 16px;
  color: #999;
`;
