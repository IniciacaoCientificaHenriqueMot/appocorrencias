import * as React from 'react';
import { AppRegistry } from 'react-native';

import Index from './src';

export default function App() {
  return <Index />;
}

AppRegistry.registerComponent('app', () => App);
